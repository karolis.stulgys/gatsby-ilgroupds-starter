import React from 'react'
import { Stack, HStack, Box, SimpleGrid } from '@chakra-ui/react'

import { Calendar } from '@irishlife/ilgroupdesignsystem.organisms.calendar'
import { Accordion } from '@irishlife/ilgroupdesignsystem.molecules.accordion'
import { PillarCard } from '@irishlife/ilgroupdesignsystem.molecules.pillar-card'
import { Alert } from '@irishlife/ilgroupdesignsystem.molecules.alert'
import { Choice } from '@irishlife/ilgroupdesignsystem.molecules.choice'
import { Dropdown } from '@irishlife/ilgroupdesignsystem.molecules.dropdown'
import { Datepicker } from '@irishlife/ilgroupdesignsystem.molecules.datepicker'
import { Breadcrumb } from '@irishlife/ilgroupdesignsystem.molecules.breadcrumb'
import { Button } from '@irishlife/ilgroupdesignsystem.atoms.button'
import { Text } from '@irishlife/ilgroupdesignsystem.atoms.text'
import { Sidebar } from '@irishlife/ilgroupdesignsystem.organisms.sidebar'
import { Navbar } from '@irishlife/ilgroupdesignsystem.organisms.navbar'
import { Avatar } from '@irishlife/ilgroupdesignsystem.atoms.avatar'
import { CheckboxGroup } from '@irishlife/ilgroupdesignsystem.molecules.checkbox-group'
import { Jumbotron } from '@irishlife/ilgroupdesignsystem.organisms.jumbotron'
import { Banner } from '@irishlife/ilgroupdesignsystem.organisms.banner'
import { Calculator } from '@irishlife/ilgroupdesignsystem.organisms.calculator'
import { BookAppointment } from '@irishlife/ilgroupdesignsystem.organisms.book-appointment'
import {
  ArrowRightCircleIcon,
  ArrowLeftCircleIcon,
  CheckIcon,
  AddIcon
} from '@irishlife/ilgroupdesignsystem.icons'

const accordionItems = [
  { id: 1, title: 'hello', text: 'world' },
  { id: 2, title: 'hello2', text: 'world2' },
  { id: 3, title: 'hello3', text: 'world3' },
]

const items = [
  { id: 1, label: 'hello', value: 'hello' },
  { id: 2, label: 'hello2', value: 'hello2' },
  { id: 3, label: 'hello3', value: 'hello3' },
]

const sidebarItems = [
  { id: 1, label: 'Overview', value: '0verview', icon: ArrowLeftCircleIcon },
  { id: 2, label: 'Health', value: 'health', icon: CheckIcon },
  {
    id: 3,
    label: 'Investments',
    value: 'investments',
    icon: AddIcon,
  },
  { id: 4, label: 'Hello', value: 'Hello', icon: AddIcon },
]

const plans = [
  {
    id: 1,
    planName: 'Net More 500 ILH',
    planItems: [
      {
        id: 1,
        label: 'Current value',
        value: '€758,831',
      },
      {
        id: 2,
        label: 'projected value at retirement',
        value: '€812,231',
      },
    ],
  },
  {
    id: 2,
    planName: 'Annual Premium',
    planItems: [
      {
        id: 1,
        label: 'Current value',
        value: '€1,321,925',
      },
      {
        id: 2,
        label: 'projected value at retirement',
        value: '€1,808,245',
      },
    ],
  },
]

export default function IndexPage() {
  return (
    <div>
      <Navbar onLogoClick={() => alert('logo clicked in Navbar!')}>
        <Button width="64" leftIcon={<Box>icon</Box>}>
          <Box as="span" width="full">
            Hello
          </Box>
        </Button>
        <Avatar />
      </Navbar>
      <Sidebar
        items={sidebarItems}
        onLogoClick={() => alert('logo clicked in Sidebar!')}
        bg="gray.100"
      >
        <Content />
      </Sidebar>
    </div>
  )
}

function MyCustomCalculator() {
  return (
    <Calculator
      title="Hello World"
      caption="hello world"
      icon={<ArrowLeftCircleIcon fontSize={54} color="vibrant.400" />}
      onStepChange={(currentStep) => console.log(currentStep)}
    >
      <Box>
        <Button>Button 1</Button>
        <Button>Button 2</Button>
      </Box>
      <Box>
        <PillarCard variant="health" plans={plans} hasConsent={true} />
      </Box>
      <Datepicker onChange={console.log} />
      <Calendar />
    </Calculator>
  )
}

const availableDates = ['03/10/2022', '03/15/2022', '03/20/2022', '04/1/2022']

const timeslots = [
  ['07:00:00', '08:00:00'],
  ['08:00:00', '09:00:00'],
  ['09:00:00', '10:00:00'],
  ['10:00:00', '11:00:00'],
  ['11:00:00', '12:00:00'],
  ['12:00:00', '13:00:00'],
]

const onSubmit = (formData) => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res({ formData })
    }, 2000)
  })
}

function getAvailableDates(): Promise<string[]> {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res(availableDates)
    }, 2000)
  })
}
// "date" is typeof Date. Will be updated to be string like - 03/01/2022
function getAvailableTimeslots(date): Promise<string[][]> {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res(timeslots)
    }, 2000)
  })
}

function Content() {
  return (
    <Stack spacing={10}>
      {/* <Checkbox>Hello</Checkbox> */}
      <BookAppointment
        onSubmit={onSubmit}
        getAvailableDates={getAvailableDates}
        getAvailableTimeslots={getAvailableTimeslots}
        isPersonDetailsKnown
        isPartnerDetailsKnown
      />
      <MyCustomCalculator />
      <SimpleGrid columns={[1, 1, 2, 3, 4]} spacing={4}>
        <Box>
          <PillarCard variant="pensions" plans={plans} hasConsent={true} />
        </Box>
        <Box>
          <PillarCard variant="health" plans={plans} hasConsent={true} />
        </Box>
        <Box>
          <PillarCard variant="investments" />
        </Box>
        <Box>
          <PillarCard variant="life" plans={plans} />
        </Box>
      </SimpleGrid>
      <Jumbotron
        variant="campaign"
        title="Hello world"
        subtitle="Hello world"
        buttonText="Go Next"
        src="https://images.unsplash.com/photo-1643635584824-993e275ef1ad?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
        renderBreadcrumb={
          <Breadcrumb items={items} onItemClick={console.log} />
        }
      />
      <Banner
        variant="dark"
        title="hello world"
        body="hello world"
        buttonText="Go Next"
      />
      <Text variant="caption-lg">Hello Team</Text>
      <Alert status="success">Hello World</Alert>
      <Dropdown options={sidebarItems} onChange={console.log} />
      <Datepicker onChange={console.log} />
      <Choice isBinary options={items} columns={[1, 3, 2]} />
      <CheckboxGroup items={items} />
      <HStack flexWrap="wrap">
        <Box>
          <Button>Normal button</Button>
        </Box>
        <Box>
          <Button size="lg">Big button</Button>
        </Box>
        <Box>
          <Button rightIcon={<ArrowRightCircleIcon fontSize={28} />}>
            Button with icon
          </Button>
        </Box>
      </HStack>
      <Accordion items={accordionItems} />
      <Calendar />
    </Stack>
  )
}
